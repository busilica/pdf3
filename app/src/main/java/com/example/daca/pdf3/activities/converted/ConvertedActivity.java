package com.example.daca.pdf3.activities.converted;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.daca.pdf3.R;
import com.example.daca.pdf3.activities.pdf.PDFBox;

public class ConvertedActivity extends AppCompatActivity implements View.OnClickListener {

    TextView textView;
    Button back, next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_converted);
        textView = (TextView) findViewById(R.id.convertedText);
        Intent intent = getIntent();
        String s = intent.getStringExtra("s");
        textView.setText(s);
        textView.setMovementMethod(new ScrollingMovementMethod());
        back = (Button) findViewById(R.id.back);
        back.setOnClickListener(this);
        next = (Button) findViewById(R.id.next);
        next.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.back:
                //textView.setText("back");
                textView.setText(PDFBox.stripCurrentPage(-1, this));
                Toast.makeText(this, "back", Toast.LENGTH_SHORT).show();
                break;
            case R.id.next:
                textView.setText(PDFBox.stripCurrentPage(1, this));
                Toast.makeText(this, "next", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
