package com.example.daca.pdf3.activities.bookmarks;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.daca.pdf3.R;
import com.example.daca.pdf3.activities.pdf.PDFActivity;

import java.util.ArrayList;

/**
 * Created by Daca on 31.01.2017..
 */

public class Adapter extends RecyclerView.Adapter<ViewHolder> {

    private Activity activity;
    private LayoutInflater inflater;
    Intent intent;

    ArrayList<String> list = new ArrayList<>();


    Adapter(BookmarksActivity activity, Intent intent) {
        this.activity = activity;
        inflater = activity.getLayoutInflater();
        list = SharedPrefHelper.getAllBookmarks(activity);
        this.intent = intent;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.activity_bookmarks_custom_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final String key = list.get(position);
        holder.textView.setText(key);
        holder.textView.setTag(position);
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent();
                int pageToLoad = SharedPrefHelper.getSelectedBookmark(key, activity);
                intent2.putExtra("bookmark", pageToLoad);
                activity.setResult(Activity.RESULT_OK, intent2);
                activity.finish();
            }
        });
        holder.textView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
                MenuItem deleteBookmark = contextMenu.add("Delete Bookmark");
                deleteBookmark.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        SharedPrefHelper.deleteSelectedBookmark(key, activity);
                        list = SharedPrefHelper.getAllBookmarks(activity);
                        notifyItemRemoved(position);
                        return true;
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
