package com.example.daca.pdf3.activities.bookmarks;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.daca.pdf3.R;

/**
 * Created by Daca on 31.01.2017..
 */

public class ViewHolder extends RecyclerView.ViewHolder {

    TextView textView;

    public ViewHolder(View itemView) {
        super(itemView);
        textView = (TextView) itemView.findViewById(R.id.textView_bookmarks_custom_row);
    }
}
