package com.example.daca.pdf3.activities.pdf;

import android.app.Activity;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.daca.pdf3.R;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.ScrollBar;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.roughike.bottombar.BottomBar;
import com.tom_roush.pdfbox.cos.COSDocument;
import com.tom_roush.pdfbox.pdmodel.PDDocument;
import com.tom_roush.pdfbox.text.PDFTextStripper;
import com.tom_roush.pdfbox.text.PDFTextStripperByArea;
import com.tom_roush.pdfbox.util.PDFBoxResourceLoader;

import java.io.File;
import java.util.Locale;

import butterknife.BindView;


public class PDFViewHelper implements OnLoadCompleteListener, OnPageChangeListener, View.OnClickListener /*TextToSpeech.OnInitListener*/ {

    Activity activity;
    private static int counter = 2;
    PDFView pdfView;
    private final TextView title;
    private final Toolbar toolbar;
    private final ScrollBar scrollBar;
    private final BottomBar bottomBar;
    private PDFView.Configurator config;
    static File file;
    TextToSpeech tts;
    int result;
    static int currentPage;

    PDFViewHelper(Activity activity, Toolbar toolbar) {
//        this.activity = activity;
        pdfView = (PDFView) activity.findViewById(R.id.pdfView);
        title = (TextView) activity.findViewById(R.id.title);
        this.toolbar = toolbar;
        scrollBar = (ScrollBar) activity.findViewById(R.id.scrollBar);
        bottomBar = (BottomBar) activity.findViewById(R.id.bottomBar);
        Intent intent = activity.getIntent();
        String path = intent.getStringExtra("filePath");
        file = new File(path);
        if (file.canRead()) {
            config = pdfView.fromFile(file);
        }
        //tts = new TextToSpeech(activity, this);
    }


    void helpWithPDFView(int pageToLoad){
            //config.pages(pageToLoad)
            config.defaultPage(pageToLoad)
                    /*.pages(5)*/
                  .onPageChange(this)
                  .onLoad(this)
                  .swipeVertical(true)
                  .load();
            pdfView.setOnClickListener(this);
            //pdfView.jumpTo(5);
            pdfView.useBestQuality(true);
            pdfView.setScrollBar(scrollBar);
            scrollBar.setHorizontal(true);
        try {

            /*if(result == TextToSpeech.LANG_NOT_SUPPORTED || result == TextToSpeech.LANG_MISSING_DATA){
               // Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show();
            }else {
                tts.speak(s, TextToSpeech.QUEUE_FLUSH, null);
                int i = 0;
            }*/


        }catch (Exception e){
            e.printStackTrace();
        }


    }

    @Override
    public void loadComplete(int nbPages) {
        toolbar.setTitle(pdfView.getCurrentPage() + " / " + pdfView.getPageCount());
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        String s = config.pages(pdfView.getCurrentPage()).toString();
        toolbar.setTitle(pdfView.getCurrentPage() + " / " + pdfView.getPageCount());
        currentPage = pdfView.getCurrentPage();
    }

    @Override
    public void onClick(View view) {
        if(counter % 2 == 0){
            toolbar.setVisibility(View.GONE);
            bottomBar.setVisibility(View.GONE);
        }else{
            toolbar.setVisibility(View.VISIBLE);
            bottomBar.setVisibility(View.VISIBLE);
        }
        counter++;
    }

    /*@Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
           result = tts.setLanguage(Locale.US);
        }
    }*/

    public static int getCurrentPage1() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public static File getFile() {
        return file;
    }

    public static void setFile(File file) {
        PDFViewHelper.file = file;
    }
}
