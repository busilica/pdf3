package com.example.daca.pdf3.entities;

import android.os.Environment;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Daca on 26.01.2017..
 */

public class PDFDoc {

    private String fileName;
    private String filePath;

    public static ArrayList<PDFDoc> getPdFsFromDownloadsFolderToArrayList() {
        ArrayList<PDFDoc> pdfDocsArrayList = new ArrayList<>();
        PDFDoc pdfDoc;
        File downloadsFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        if (downloadsFolder.exists()){
            File[] files = downloadsFolder.listFiles();
            for (int i = 0; i < files.length; i++){
                File file = files[i];
                if (file.getPath().endsWith("pdf")){
                    pdfDoc = new PDFDoc();
                    pdfDoc.setFileName(file.getName());
                    pdfDoc.setFilePath(file.getAbsolutePath());
                    pdfDocsArrayList.add(pdfDoc);
                }
            }
        }
        return pdfDocsArrayList;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
