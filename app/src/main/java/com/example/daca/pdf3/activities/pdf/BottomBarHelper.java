package com.example.daca.pdf3.activities.pdf;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.example.daca.pdf3.R;
import com.example.daca.pdf3.activities.bookmarks.SharedPrefHelper;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;



class BottomBarHelper {

    static void setupBottomBar(BottomBar bottomBar, final AppCompatActivity activity){
        bottomBar.setDefaultTab(R.id.tab_tts);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch (tabId){
                    case R.id.tab_bookmarks:
                        Toast.makeText(activity, "clicked on bookmarks", Toast.LENGTH_SHORT).show();
                        SharedPrefHelper.saveNewBookmark(activity);
                        break;
                    case R.id.tab_tts:
                        Toast.makeText(activity, "clicked on tts", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.tab_settings:
                        Toast.makeText(activity, "clicked on settings", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        bottomBar.setOnTabReselectListener(new OnTabReselectListener() {
            @Override
            public void onTabReSelected(@IdRes int tabId) {
                switch (tabId){
                    case R.id.tab_bookmarks:
                        if(SharedPrefHelper.saveNewBookmark(activity)){
                            Toast.makeText(activity, "Bookmark Saved", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case R.id.tab_tts:
                        Toast.makeText(activity, "clicked on tts", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.tab_settings:
                        Toast.makeText(activity, "clicked on settings", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

    }



}
