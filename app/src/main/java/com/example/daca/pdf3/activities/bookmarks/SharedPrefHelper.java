package com.example.daca.pdf3.activities.bookmarks;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.example.daca.pdf3.activities.pdf.PDFViewHelper;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Daca on 31.01.2017..
 */

public class SharedPrefHelper {

    public static boolean saveNewBookmark(Activity activity){
        int currentPage = PDFViewHelper.getCurrentPage1();
        SharedPreferences sharedPreferences = activity.getSharedPreferences("Bookmarks", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("page " + currentPage, currentPage);
        editor.apply();
        return true;
    }

    public static int getSelectedBookmark(String key, Activity activity){
        int currentPage = 0;
        SharedPreferences sharedPreferences = activity.getSharedPreferences("Bookmarks", Context.MODE_PRIVATE);
        currentPage = sharedPreferences.getInt(key, 0);
        return currentPage;
    }

    public static ArrayList<String> getAllBookmarks(Activity activity){
        ArrayList<String> bookmarks = new ArrayList<>();

        SharedPreferences sharedPreferences = activity.getSharedPreferences("Bookmarks", Context.MODE_PRIVATE);
        Map<String, ?> allBookmarks = sharedPreferences.getAll();
        for (Map.Entry<String, ?> entry : allBookmarks.entrySet()){
            bookmarks.add(entry.getKey());
        }

        return bookmarks;
    }

    public static void deleteSelectedBookmark(String key, Activity activity){
        SharedPreferences sharedPreferences = activity.getSharedPreferences("Bookmarks", Context.MODE_PRIVATE);
        sharedPreferences.edit().remove(key).commit();
        Toast.makeText(activity, "Bookmark removed", Toast.LENGTH_SHORT).show();
    }
}
