package com.example.daca.pdf3.activities.main;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.daca.pdf3.R;


class ViewHolder extends RecyclerView.ViewHolder {

   ImageView imageViewPdfImage;
   TextView textViewPdfName;

    ViewHolder(View itemView) {
        super(itemView);
        imageViewPdfImage = (ImageView) itemView.findViewById(R.id.imageView_custom_row_main);
        textViewPdfName = (TextView) itemView.findViewById(R.id.textView_custom_row_main);
    }
}
