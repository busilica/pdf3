package com.example.daca.pdf3.activities.main;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.daca.pdf3.activities.pdf.PDFActivity;
import com.example.daca.pdf3.R;
import com.example.daca.pdf3.entities.PDFDoc;

import java.util.ArrayList;


public class Adapter extends RecyclerView.Adapter<ViewHolder> {

    private Activity activity;
    private ArrayList<PDFDoc> pdfDocsArrayList;
    private LayoutInflater inflater;

    Adapter(Activity activity, ArrayList<PDFDoc> pdfDocsArrayList) {
        this.activity = activity;
        this.pdfDocsArrayList = pdfDocsArrayList;
        inflater = activity.getLayoutInflater();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.custom_row_main, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.textViewPdfName.setText(pdfDocsArrayList.get(position).getFileName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, PDFActivity.class);
                intent.putExtra("filePath", pdfDocsArrayList.get(position).getFilePath());
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pdfDocsArrayList.size();
    }
}
