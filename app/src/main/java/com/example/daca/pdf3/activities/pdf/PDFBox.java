package com.example.daca.pdf3.activities.pdf;

import android.app.Activity;
import android.app.ProgressDialog;

import com.tom_roush.pdfbox.pdmodel.PDDocument;
import com.tom_roush.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.IOException;


public class PDFBox {

    static ProgressDialog dialog;

    public static String stripCurrentPage(int c, Activity activity) {
        dialog = new ProgressDialog(activity);
        dialog.setTitle("Processing...");
        dialog.setMessage("please wait");
        dialog.show();
        int currentPage = PDFViewHelper.getCurrentPage1() + c;
        File file = PDFViewHelper.getFile();
        String s = null;
        PDDocument doc = null;
        try {
            doc = PDDocument.load(file);
            doc.getNumberOfPages();
            PDFTextStripper stripper = new PDFTextStripper();
            stripper.setStartPage(currentPage);
            stripper.setEndPage(currentPage + 1);
            s = stripper.getText(doc);
            //doc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        dialog.dismiss();
        return s;
    }
}
