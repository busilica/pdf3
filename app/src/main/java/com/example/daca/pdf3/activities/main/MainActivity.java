package com.example.daca.pdf3.activities.main;

import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.daca.pdf3.R;
import com.example.daca.pdf3.entities.PDFDoc;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button button;
    ImageButton navButton;
    RecyclerView recyclerView;
    ArrayList<PDFDoc> pdfDocsArrayList = new ArrayList<>();
    Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.button_main);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_main);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        pdfDocsArrayList = PDFDoc.getPdFsFromDownloadsFolderToArrayList();
        adapter = new Adapter(this, pdfDocsArrayList);
        recyclerView.setAdapter(adapter);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://www.bookzz.org";
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(MainActivity.this, Uri.parse(url));
            }
        });


    }


}
